type Variadic<T extends readonly any[], U = any> = (...args: T) => U;
type Head<T extends readonly any[]> = Variadic<T> extends (arg: infer U, ...args: any[]) => any
  ? U
  : never;
type Tail<T extends readonly any[]> = Variadic<T> extends (arg: any, ...args: infer U) => any
  ? U
  : never;
type Init<T extends readonly any[]> = Variadic<T> extends (...args: [...infer U, any]) => any
  ? U
  : never;
type HasIndex<T extends readonly any[], U extends number> = `${U}` extends keyof T
  ? true
  : false;

type Skips<
  T extends any[],
  U extends number,
  V extends readonly any[] = [],
  X = unknown
> = HasIndex<V, U> extends true ? [X, ...T] : T extends [] ? T : Skips<Tail<T>, U, [...V, X], Head<T>>;

type Skip<T extends any[], U extends number> = Skips<T, U>;