type KnownTypes = number | string | boolean | null;

type List<T> = T | T[];

type AnyJson =
  | {
      [key: string]: List<AnyJson>;
    }
  | List<KnownTypes>;

type Json<T extends AnyJson = AnyJson> = T extends AnyJson ? T : never;

// valid
type ValidPrimitives = Json<KnownTypes[]>;
type KnownJson = {
  n: number;
  s: string;
  b: boolean;
  ø: null;
  arrayOfPrimitives: KnownTypes[];
  object: AnyJson;
  arrayOfJsons: AnyJson[];
};

const vp = [1, '1', true, null];
const validPrimitives: AnyJson[] = vp;
const validPrimitives2: ValidPrimitives = vp;
const emptyObject: AnyJson = {};
const object: AnyJson = { n: 1, s: '1', b: true, ø: null };
const nestedObject: AnyJson = { o: object, a: [object, object], ...object };
const knownJson: Json<KnownJson> = {
  n: 42,
  s: '42',
  b: true,
  ø: null,
  arrayOfPrimitives: vp,
  object,
  arrayOfJsons: [object, object]
};

// invalid
const nil: AnyJson = undefined; //need strictNullChecks for this one to work
const symbol: AnyJson = Symbol('42');
const withSymbolKey: AnyJson = { [symbol]: object };
const withNilField: AnyJson = { n: undefined };
const withNilOnlyArray: AnyJson = { a: [undefined] };
const withSymbolOnlyArray: AnyJson = { a: [Symbol('42')] };
const withNtuple: AnyJson = { a: [Symbol('42'), 42] };
