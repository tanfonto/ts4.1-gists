type Variadic<T extends readonly any[], U = any> = (...args: T) => U;

type Head<T extends readonly any[]> = Variadic<T> extends (
  arg: infer U,
  ...args: any[]
) => any
  ? U
  : never;

type Tail<T extends readonly any[]> = Variadic<T> extends (
  arg: any,
  ...args: infer U
) => any
  ? U
  : never;

type Split<T extends readonly any[], U extends boolean> = Variadic<T> extends (
  ...args: [...infer V, infer X]
) => any
  ? U extends true
    ? V
    : X
  : never;

type Init<T extends readonly any[]> = Split<T, true>;

type Last<T extends readonly any[]> = Split<T, false>;

type Indices<T extends readonly any[]> = Exclude<keyof T, keyof Array<any>>;

type AsNegativeIndices<T extends readonly any[]> = keyof {
  [K in Indices<T> as `-${string & K}`]: any;
};

type TakesLast<
  T extends readonly any[],
  U extends number,
  V extends readonly any[] = []
> = `-${V['length']}` extends `${U}`
  ? V
  : TakesLast<Init<T>, U, [Last<T>, ...V]>;

type TakeLast<T extends readonly any[], U extends number> = TakesLast<T, U>;

type Skips<
  T extends readonly any[],
  U extends number,
  V extends readonly any[] = []
> = `${U}` extends AsNegativeIndices<T>
  ? TakeLast<T, U>
  : V['length'] extends U
  ? T
  : Skips<Tail<T>, U, [...V, Head<T>]>;

type Skip<T extends readonly any[], U extends number> = Skips<T, U>;

type Takes<
  T extends readonly any[],
  U extends number,
  V extends readonly any[] = [],
  X = T
> = T['length'] extends U
  ? T
  : T['length'] extends 0
  ? V['length'] extends U
    ? V
    : X
  : `${U}` extends AsNegativeIndices<T>
  ? `-${V['length']}` extends `${U}`
    ? V
    : Takes<Tail<T>, U, [...V, Head<T>], X>
  : Takes<Init<T>, U, V, X>;

type Take<T extends readonly any[], U extends number> = Takes<T, U>;

type Slice<
  T extends any[],
  U extends number = 0,
  V extends number = 0
> = V extends 0 ? Skip<T, U> : Skip<Take<T, V>, U>;

type Animals = ['ant', 'bison', 'camel', 'duck', 'elephant'];

type Skept = Skip<Animals, 2>;

type SkeptNegative = Skip<Animals, -2>;

type SkeptOverflow = Skip<Animals, 10>;

type Taken = Take<Animals, 3>;

type TakenNegative = Take<Animals, -3>;

type TakenOverflow = Take<Animals, 10>;

type EndsWith = Last<Animals>;

type Sliced = Slice<Animals, 2, 4>;

type SlicedNoUpperbound = Slice<Animals, 2>;

type SlicedNoBounds = Slice<Animals>;

type SlicedLowerboundOverflow = Slice<Animals, 10>;

type SlicedUpperboundOverflow = Slice<Animals, 2, 10>;

type SlicedNegativeLowerbound = Slice<Animals, -2>;

type SlicedNegativeUpperbound = Slice<Animals, 0, -3>;

type SlicedNegativeBoth = Slice<Animals, -1, -3>;
