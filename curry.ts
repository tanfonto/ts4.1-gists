type Variadic<T extends readonly any[], U = any> = (...args: T) => U;

type Curried<T> = T extends (head: infer U, ...tail: infer V) => any
  ? V extends []
    ? (arg: U) => ReturnType<T>
    : (arg: U) => Curried<(...args: V) => ReturnType<T>>
    : never;

declare function curry<T extends any[], U, F extends Variadic<T, U>>(
    f: F
): Curried<F>;

declare function f(
  arg0: string,
  arg1: number,
  arg2: boolean
): Record<string, any>;

const curried = curry(f);

const f2 = curried('42');
