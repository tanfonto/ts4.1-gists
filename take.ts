type Variadic<T extends readonly any[], U = any> = (...args: T) => U;

type Init<T extends readonly any[]> = Variadic<T> extends (...args: [...infer U, unknown]) => any
  ? U
  : never
  
type HasIndex<T extends readonly any[], U extends number> = `${U}` extends keyof T
  ? true
  : false;

type Take<T extends any[], U extends number> = HasIndex<T, U> extends true ? Take<Init<T>, U> : T;

type Taken = Take<[0, 1, 2, 3, 4], 2>;

